"""Trains and Evaluates the MNIST network using a feed dictionary."""
# pylint: disable=missing-docstring
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import time

from six.moves import xrange  # pylint: disable=redefined-builtin
import tensorflow as tf

from data import read_data_sets
import deepfx


# Basic model parameters as external flags.
flags = tf.app.flags
FLAGS = flags.FLAGS
flags.DEFINE_float('learning_rate', 0.01, 'Initial learning rate.')
flags.DEFINE_integer('max_steps', 200, 'Number of steps to run trainer.')
flags.DEFINE_integer('hidden1', 4, 'Number of units in hidden layer 1.')
flags.DEFINE_integer('hidden2', 3, 'Number of units in hidden layer 2.')
flags.DEFINE_integer('batch_size', 10, 'Batch size.  '
                     'Must divide evenly into the dataset sizes.')
flags.DEFINE_string('train_dir', 'train', 'Directory to put the training data.')
flags.DEFINE_boolean('fake_data', False, 'If true, uses fake data '
                     'for unit testing.')


def placeholder_inputs(batch_size):
  """Generate placeholder variables to represent the input tensors.
  These placeholders are used as inputs by the rest of the model building
  code and will be fed from the downloaded data in the .run() loop, below.
  Args:
    batch_size: The batch size will be baked into both placeholders.
  Returns:
    images_placeholder: Images placeholder.
    labels_placeholder: Labels placeholder.
  """
  # Note that the shapes of the placeholders match the shapes of the full
  # image and label tensors, except the first dimension is now batch_size
  # rather than the full size of the train or test data sets.
  images_placeholder = tf.placeholder(tf.float32, [None, deepfx.NUM_INPUT])
  labels_placeholder = tf.placeholder(tf.float32, [None, deepfx.NUM_CLASSES])
  return images_placeholder, labels_placeholder


def fill_feed_dict(data_set, input_pl, label_pl):
  input = []
  label = []
  for i in xrange(FLAGS.batch_size):
      d = data_set.next()
      input.append(d[0])
      label.append(d[1])
  #print(input)
  #print(label)
  return {input_pl: input,
          label_pl: label}


def do_eval(sess,
            eval_correct,
            images_placeholder,
            labels_placeholder,
            data_set):
  """Runs one evaluation against the full epoch of data.
  Args:
    sess: The session in which the model has been trained.
    eval_correct: The Tensor that returns the number of correct predictions.
    images_placeholder: The images placeholder.
    labels_placeholder: The labels placeholder.
    data_set: The set of images and labels to evaluate, from
      input_data.read_data_sets().
  """
  # And run one epoch of eval.
  true_count = 0  # Counts the number of correct predictions.
  steps_per_epoch = data_set.num_examples // FLAGS.batch_size
  num_examples = steps_per_epoch * FLAGS.batch_size
  for step in xrange(steps_per_epoch):
    feed_dict = fill_feed_dict(data_set,
                               images_placeholder,
                               labels_placeholder)
    true_count += sess.run(eval_correct, feed_dict=feed_dict)
  precision = true_count / num_examples
  print('  Num examples: %d  Num correct: %d  Precision @ 1: %0.04f' %
        (num_examples, true_count, precision))


def run_training():
  data_sets = read_data_sets()

  with tf.Graph().as_default():
    input_placeholder, label_placeholder = placeholder_inputs(
        FLAGS.batch_size)

    logits = deepfx.inference(input_placeholder,
                             FLAGS.hidden1,
                             FLAGS.hidden2)
    loss = deepfx.loss(logits, label_placeholder)
    train_op = deepfx.training(loss, FLAGS.learning_rate)
    #eval_label = deepfx.evaluation(logits, label_placeholder)
    summary_op = tf.merge_all_summaries()
    init = tf.initialize_all_variables()
    saver = tf.train.Saver()
    sess = tf.Session()
    summary_writer = tf.train.SummaryWriter(FLAGS.train_dir, graph_def=sess.graph_def) #sess.graph)
    sess.run(init)

    for step in xrange(FLAGS.max_steps):
      start_time = time.time()
      feed_dict = fill_feed_dict(data_sets,
                                 input_placeholder,
                                 label_placeholder)

      _, loss_value = sess.run([train_op, loss],
                               feed_dict=feed_dict)
      duration = time.time() - start_time
      if step % 100 == 0:
        print('Step %d: loss = %.2f (%.3f sec)' % (step, loss_value, duration))
        continue
        summary_str = sess.run(summary_op, feed_dict=feed_dict)
        summary_writer.add_summary(summary_str, step)
        summary_writer.flush()

#      # Save a checkpoint and evaluate the model periodically.
#      if (step + 1) % 1000 == 0 or (step + 1) == FLAGS.max_steps:
#        saver.save(sess, FLAGS.train_dir, global_step=step)
#        # Evaluate against the training set.
#        print('Training Data Eval:')
#        do_eval(sess,
#                eval_correct,
#                images_placeholder,
#                labels_placeholder,
#                data_sets.train)
#        # Evaluate against the validation set.
#        print('Validation Data Eval:')
#        do_eval(sess,
#                eval_correct,
#                images_placeholder,
#                labels_placeholder,
#                data_sets.validation)
#        # Evaluate against the test set.
#        print('Test Data Eval:')
#        do_eval(sess,
#                eval_correct,
#                images_placeholder,
#                labels_placeholder,
#                data_sets.test)


def main(_):
  run_training()


if __name__ == '__main__':
  tf.app.run()
